#ifndef IMAGE_TRANSFORMER_TRANSFORMS_H
#define IMAGE_TRANSFORMER_TRANSFORMS_H

// To define a transformation:
// - add it's transcode
// - create structure, containing its arguments
// - insert that structure in transformation union
//
// - define transforming method in its own module
// - insert that method in TRANSFORMATIONS array by transcode
//
// It is also preferred to write your own methods to simplify
//  concrete transformation instance creation.


enum transcode {
    ROTATE = 0
};

struct transform;
typedef struct image (*transforming)(struct transform self, struct image);

// descriptor of rotation
struct rotation {
    int64_t angle;
};

union transformation {
    struct rotation as_rotation;
};

struct transform {
    enum transcode code;
    union transformation view;
    char* verdict;
};

struct image transform_image(struct transform transform, struct image image);

#endif //IMAGE_TRANSFORMER_TRANSFORMS_H
