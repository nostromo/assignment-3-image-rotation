#ifndef IMAGE_TRANSFORMER_FORMATS_H
#define IMAGE_TRANSFORMER_FORMATS_H

#include "bmp.h"
#include "image.h"

enum format {
    BMP = 0
};

char const* convert_to_format(struct image const* image, FILE* file, enum format format);
char const* convert_from_format(struct image* image, FILE* file, enum format format);

#endif //IMAGE_TRANSFORMER_FORMATS_H
