#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include <image.h>
#include <stddef.h>
#include <stdio.h>

char const* convert_to_bmp(FILE* file, struct image const* image);
char const* convert_from_bmp(FILE* file, struct image* image);

#endif //IMAGE_TRANSFORMER_BMP_H
