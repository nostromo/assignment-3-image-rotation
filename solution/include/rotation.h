#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H
#define ANGLE_STEP 90
#define MAX_TURNS_CLOCKWISE 3
#define MAX_TURNS_COUNTER 3
#define CIRCLE 360

#include "transforms.h"
#include <stdint.h>

struct image rotate(struct transform self, struct image image);

struct transform build_rotation(int64_t angle);

#endif //IMAGE_TRANSFORMER_ROTATION_H
