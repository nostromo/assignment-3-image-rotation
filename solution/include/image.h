#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>



struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image image_create(uint64_t width, uint64_t height);
void image_destroy(struct image image);
bool image_valid(struct image image);

#endif //IMAGE_TRANSFORMER_IMAGE_H
