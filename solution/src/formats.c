#include "bmp.h"
#include "formats.h"
#include "image.h"
#include <stdio.h>

typedef char const* (*converting_to)(FILE*, struct image const*);
typedef char const* (*converting_from)(FILE*, struct image*);

struct converter {
    converting_to to_format;
    converting_from from_format;
};

static const struct converter CONVERTERS[] = {[BMP] = {convert_to_bmp, convert_from_bmp}};

char const* convert_to_format(struct image const* image, FILE* file, enum format format) {
    return CONVERTERS[format].to_format(file,image);
}
char const* convert_from_format(struct image* image, FILE* file, enum format format) {
    return CONVERTERS[format].from_format(file,image);
}

