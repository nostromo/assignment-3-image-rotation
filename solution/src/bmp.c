#include "bmp.h"
#include "image.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#define BF_TYPE 19778
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_PELS_PER_METER 2834
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0

#define ROW_ALIGN 4

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FAILED_IMAGE
};
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
};

static size_t calculate_padding(uint64_t width) {
    return (ROW_ALIGN - width * sizeof(struct pixel) % ROW_ALIGN) % ROW_ALIGN;
}

// Inner process of reading image from a bmp file
// non-static, so can be used directly from other modules, but not recommended
enum read_status from_bmp( FILE* in, struct image* img ) {

    // !!!
    // method doesn't exit the program, only returns status,
    // so memory is freed up in external functions


    //  try reading header
    struct bmp_header header = {0};
    size_t header_reading = fread(&header, sizeof(struct bmp_header), 1, in);

    if (header_reading!=1) return READ_INVALID_HEADER;
    if (header.bfType != BF_TYPE || header.biBitCount != BI_BIT_COUNT) return READ_INVALID_SIGNATURE;

    // move on to pixel bytes
    if (fseek(in, (long)header.bOffBits, SEEK_SET)) return READ_INVALID_HEADER;

    // try initialising image
    *img = image_create(header.biWidth, header.biHeight);
    if (!image_valid(*img)) return READ_FAILED_IMAGE;

    // fill image pixels by rows with padding
    size_t padding = calculate_padding(img->width);
    size_t row_size = sizeof(struct pixel) * img->width;
    struct pixel* current = img->data;
    for (size_t row = 0; row < header.biHeight; row++) {
        // try writing row
        size_t row_reading = fread(current,row_size, 1, in);
        if (row_reading!=1) return READ_INVALID_BITS;

        // skip padding
        if (fseek(in,(long)padding,SEEK_CUR)) return READ_INVALID_BITS;

        current += img->width;
    }
    return READ_OK;
}

// Forms bmp_header based on the image structure.
static struct bmp_header bmp_form_header(struct image image) {
    struct bmp_header header = {.bfType=BF_TYPE,.bfReserved=BF_RESERVED,.biSize=BI_SIZE,.biPlanes=BI_PLANES,
            .biBitCount=BI_BIT_COUNT,.biCompression=BI_COMPRESSION,.biXPelsPerMeter=BI_PELS_PER_METER,
            .biYPelsPerMeter=BI_PELS_PER_METER,.biClrUsed=BI_CLR_USED,.biClrImportant=BI_CLR_IMPORTANT};

    size_t padding = calculate_padding(image.width);
    size_t header_size = sizeof(struct bmp_header);
    size_t data_size = (image.width*sizeof(struct pixel)+padding)*image.height;

    header.bfileSize = header_size + data_size;
    header.bOffBits = header_size;
    header.biWidth = image.width;
    header.biHeight = image.height;
    header.biSizeImage = data_size;

    return header;
}

// Inner process of writing image to a bmp file
// non-static, so can be used directly from other modules, but not recommended
enum write_status to_bmp( FILE* out, struct image const* const image ) {

    // !!!
    // method doesn't exit the program, only returns status,
    // so memory is freed up in external functions


    struct bmp_header header = bmp_form_header(*image);
    size_t padding = calculate_padding(image->width);

    size_t header_writing = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (header_writing!=1) return WRITE_ERROR;

    size_t row_size = sizeof(struct pixel) * image->width;
    struct pixel* current = image->data;
    for (size_t row = 0; row < header.biHeight; row++) {
        // try writing row
        size_t row_writing = fwrite(current,row_size,1,out);
        if (row_writing!=1) return WRITE_ERROR;

        // skip padding
        if (fseek(out,(long)padding,SEEK_CUR)) return WRITE_ERROR;

        current += image->width;
    }

    return WRITE_OK;
}

// Turns inner reading status ENUM into a string
char const* bmp_interpret_read_status(enum read_status status) {
    switch (status) {
        // can be packed into a char* array like {[READ_INVALID_SIGNATURE]="message"...},
        // but this is easier to read & there is a default option
        case READ_INVALID_SIGNATURE: return "Wrong signature";
        case READ_INVALID_BITS: return "Wrong bits";
        case READ_INVALID_HEADER: return "Wrong header";
        case READ_FAILED_IMAGE: return "Failed to place the image in memory";
        default: return 0;
    }
}
// Turns inner writing status ENUM into a string
char const* bmp_interpret_write_status(enum write_status status) {
    switch (status) {
        case WRITE_ERROR: return "Failed to write the image to a file";
        default: return 0;
    }
}

// Reads image from a bmp file
// Returns error message or NULL if everything's fine
// Char array messages are used to make converting functions
//  for each format look less dependent on the internal representation
//  of errors.
char const* convert_from_bmp(FILE* file, struct image* image) {
    enum read_status status = from_bmp(file, image);
    return bmp_interpret_read_status(status);
}

// Writes image to a bmp file
// Returns error message or NULL if everything's fine
char const* convert_to_bmp(FILE* file, struct image const* const image) {
    enum write_status status = to_bmp(file, image);
    return bmp_interpret_write_status(status);
}




