#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
    struct image created = {0};
    created.width = width;
    created.height = height;
    created.data = malloc(sizeof(struct pixel)*width*height);
    return created;
}

bool image_valid(struct image image) {
    return (image.data);
}

void image_destroy(struct image image) {
    if (image_valid(image)) free(image.data);
    image.data = NULL;
}
