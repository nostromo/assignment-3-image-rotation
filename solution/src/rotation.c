#include "image.h"
#include "rotation.h"
#include "transforms.h"
#include <stddef.h>
#include <stdint.h>


static struct image rotate_90(struct image image) {
    struct image rotated = image_create(image.height, image.width);
    // invalid images are detected in the general
    //  transform_image function.
    // but every concrete transformation function
    //  such as rotate, can detect them themselves
    //  and set their own error message to the transform
    if (!image_valid(rotated)) return rotated;
    struct pixel pixel;
    for (size_t x = 0; x < rotated.height; x++) {
        for (size_t y = 0; y < rotated.width; y++) {
            pixel = image.data[y * image.width + x];
            rotated.data[(image.width-x-1)*image.height+y] = pixel;
        }
    }
    return rotated;
}

static struct image rotate_180(struct image image) {
    struct image rotated = image_create(image.width, image.height);
    if (!image_valid(rotated)) return rotated;
    for (size_t x=0; x<image.width; x++) {
        for (size_t y=0; y<image.height; y++) {
            rotated.data[(rotated.width)*(image.height-y)-x-1] = image.data[image.width*y+x];
        }
    }
    return rotated;
}

static struct image rotate_270(struct image image) {
    struct image rotated = image_create(image.height, image.width);
    if (!image_valid(rotated)) return rotated;
    struct pixel pixel;
    for (size_t x = 0; x < rotated.height; x++) {
        for (size_t y = 0; y < rotated.width; y++) {
            pixel = image.data[y * image.width + x];
            rotated.data[(x+1)*image.height-y-1] = pixel;
        }
    }
    return rotated;
}

struct image rotate(struct transform self, struct image image) {
    int64_t angle = self.view.as_rotation.angle;
    int64_t turns = ((CIRCLE + angle) % CIRCLE) / ANGLE_STEP;
    switch (turns) {
        case 1: return rotate_90(image);
        case 2: return rotate_180(image);
        case 3: return rotate_270(image);
        default: return image;
    }
}



struct transform build_rotation(int64_t angle) {
    struct transform transform = {0};
    transform.code = ROTATE;
    transform.view = (union transformation){.as_rotation = {.angle=angle}};
    return transform;
}
