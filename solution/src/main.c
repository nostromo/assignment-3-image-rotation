#include "formats.h"
#include "image.h"
#include "rotation.h"
#include "transforms.h"
#include <stdio.h>
#include <stdlib.h>

#define ERROR_LOG stderr
#define ERRCODE_ANGLE 1
#define ERRCODE_CONVERT_TO 2
#define ERRCODE_CONVERT_FROM 3
#define ERRCODE_TRANSFORM 4
#define ERRCODE_OPEN 5
#define ANGLE_BASE 10

int main( int argc, char** argv ) {
    if (argc != 4) {
        fprintf(ERROR_LOG, "The number of arguments is incorrect.\n");
        fprintf(ERROR_LOG, "Expected: <source-image> <transformed-image> <angle>\n");
        exit(-1);
    }

    char* strtol_end = 0;
    long angle = strtol(argv[3],&strtol_end,ANGLE_BASE);
    if (angle % ANGLE_STEP != 0) {
        fprintf(ERROR_LOG, "Only angles that are multiples of 90 are allowed.\n");
        exit(ERRCODE_ANGLE);
    } else {
        if (angle / ANGLE_STEP < -MAX_TURNS_CLOCKWISE || angle / ANGLE_STEP > MAX_TURNS_COUNTER) {
            fprintf(ERROR_LOG, "Only angles from the list are allowed:\n");
            fprintf(ERROR_LOG, "0, 90, -90, 180, -180, 270, -270");
            exit(ERRCODE_ANGLE);
        }
    }

    // open_source_file
    const char* source = argv[1];
    FILE* source_file = fopen(source, "rb");
    if (!source_file) {
        fprintf(ERROR_LOG, "Failed to open source file \"%s\"\n", source);
        exit(ERRCODE_OPEN);
    }

    // try reading the image
    struct image image = {0};
    char const* converting_from_verdict = convert_from_format(&image, source_file, BMP);
    fclose(source_file);

    // some error before/while the image was being created
    if (!image_valid(image)) {
        fprintf(ERROR_LOG, "Failed to create an image\n");
        if (converting_from_verdict) fprintf(ERROR_LOG, "%s\n", converting_from_verdict);
        exit(-1);
    }

    // some error after successful image initialization
    if (converting_from_verdict) {
        image_destroy(image);
        fprintf(ERROR_LOG, "Failed to read from the format: %s\n", converting_from_verdict);
        exit(ERRCODE_CONVERT_FROM);
    }

    struct transform rotation = build_rotation(angle);
    struct image transformed = transform_image(rotation, image);

    // Program doesn't recreate the image, when angle equals 0
    // image and transformed can refer to the same data
    //  so image is closed only if it's different from the transformed one
    // (an alternative to creating a deep copy)
    if (image.data != transformed.data) image_destroy(image);

    // handles transform errors:
    if (!image_valid(transformed)) {
        image_destroy(transformed);
        fprintf(ERROR_LOG, "Transformation failed\n");
        if (rotation.verdict) fprintf(ERROR_LOG, "%s\n", rotation.verdict);
        exit(ERRCODE_TRANSFORM);
    }

    // open_result_file
    const char* result = argv[2];
    FILE* result_file = fopen(result, "wb");
    if (!result_file) {
        fprintf(ERROR_LOG, "Failed to open result file \"%s\"\n", result);
        exit(ERRCODE_OPEN);
    }

    // try writing the image to a file
    char const* converting_to_verdict = convert_to_format(&transformed, result_file, BMP);

    // even if failed, clear everything up
    fclose(result_file);
    image_destroy(transformed);

    // handle writing error
    if (converting_to_verdict) {
        fprintf(ERROR_LOG, "%s\n", converting_to_verdict);
        exit(ERRCODE_CONVERT_TO);
    }
    return 0;
}
