#include "image.h"
#include "rotation.h"
#include "transforms.h"

static const transforming TRANSFORMATIONS[] = {
    [ROTATE] = rotate
};

struct image transform_image(struct transform transform, struct image image) {
    struct image transformed = TRANSFORMATIONS[transform.code](transform,image);
    if (!image_valid(transformed)) {
        // if no error message was formed by concrete transformation
        //  but image is invalid, add general error message;
        if (!transform.verdict) transform.verdict = "Failed to create image";
    }
    return transformed;
}




